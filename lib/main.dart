import 'package:flutter/material.dart';
import 'package:recette_cuisine/views/home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Recette de cuisine',
      theme: ThemeData(
        primaryColor: Colors.white,
        textTheme: TextTheme(
          bodyText2: TextStyle(color: Colors.white)
        ),
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}


