import 'package:flutter/material.dart';
import 'package:recette_cuisine/models/recette.api.dart';
import 'package:recette_cuisine/models/recette.dart';
import 'package:recette_cuisine/views/widgets/recette_card.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<Recette> _recettes;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    getRecipes();
  }

  Future<void> getRecipes() async {
    _recettes = await RecetteApi.getRecipe();
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.restaurant_menu),
              SizedBox(width: 10),
              Text('Recette de Cuisine')
            ],
          ),
        ),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
          itemCount: _recettes.length,
          itemBuilder: (context, index) {
            return RecetteCarte(
                title: _recettes[index].name,
                cookTime: _recettes[index].totalTime,
                rating: _recettes[index].rating.toString(),
                thumbnailUrl: _recettes[index].images);
          },
        ));
  }
}