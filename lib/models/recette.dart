class Recette {
  final String name;
  final String images;
  final double rating;
  final String totalTime;

  Recette({required this.name, required this.images, required this.rating, required this.totalTime});

  factory Recette.fromJson(dynamic json) {
    return Recette(
        name: json['name'] as String,
        images: json['images'][0]['hostedLargeUrl'] as String,
        rating: json['rating'] as double,
        totalTime: json['totalTime'] as String);
  }

  static List<Recette> recipesFromSnapshot(List snapshot) {
    return snapshot.map((data) {
      return Recette.fromJson(data);
    }).toList();
  }

  @override
  String toString(){
    return 'Recipe {name: $name, image: $images, rating: $rating, totalTime: $totalTime}';
  }
}